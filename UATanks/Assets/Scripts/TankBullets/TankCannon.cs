﻿//Norman Nguyen
//Tank Cannon: Adds the position of the firing point from the tank to allow the bullet to spawn.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCannon : MonoBehaviour
{
    //Tank Data, FireTime, and Tank Firepoint position.
    private TankData data;
    private float FiringTime;
    public Transform TankFirepoint;

    void Start()
    {
        //Start data and firing time
        data = GetComponent<TankData>();
        FiringTime = Time.time;
    }
    //Fire Method to allow the bullet to fire from its
    public void Fire()
    {
        if (Time.time > FiringTime)
        {
            //Instantiate the position of the bullet to be spawned
            GameObject bullet = Instantiate(data.bulletPrefab , TankFirepoint.position, TankFirepoint.rotation) as GameObject;
            //Get bullet data component
            BulletData bulletData = bullet.GetComponent<BulletData>();
            //bullet score
            bulletData.currentScore = data.gameObject;
            //Bullet Data speed and Tank Data speed rate to help the tank bullet speed.
            bulletData.speed = data.speedRate;
            //Counts the time of fire based on the shooting rate from the data
            FiringTime = Time.time + data.shootRate;
        }
    }
}