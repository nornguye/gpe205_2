﻿//Norman Nguyen
//Script/Component: Tank Motor for movement.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    //CharacterController
    private CharacterController cc;

    //Transform with tf instance
    [HideInInspector] public Transform tf;

    //Get Tank Data
    private TankData data;

    void Start()
    {
        // Get components for character controller (input), transform, and tank data.
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
    }

    //Moves the tank forward
    public void Move(Vector3 speedVector)
    {

        speedVector = transform.forward * data.moveSpeed;
        //SimpleMove()
        cc.SimpleMove(speedVector);
    }

    //Moves the tank backwards for reverse speed
    public void Reverse(Vector3 speedVector)
    {
        speedVector = transform.forward * data.moveSpeed;
        //SimpleMove()
        cc.SimpleMove(-speedVector);
    }

    //This is for your tank rotation
    public void Turn(float direction)
    {
        //Rotate()
        tf.Rotate(0, Mathf.Sign(direction) * data.turnSpeed * Time.deltaTime, 0);
    }

    //Rotate Towards the player.
    public void TurnTowards(Vector3 target)
    {
        // Find the Quaternion that looks down that vector
        Quaternion targetRotation = Quaternion.LookRotation(target);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);

    }
}
