﻿//Norman Nguyen
//Tank Data: This is everything the Tank has had from movements, firing, and points data.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Requires component for tank and cannon to get the tank working.
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankCannon))]
public class TankData : MonoBehaviour
{
    public float moveSpeed; // In meters per second
    public float reverseSpeed; // Reverse speed for the tank
    public float turnSpeed; // In degrees per second
    public float shootRate; // Adjust the speed of the bullet from rapid or slow timing.
    public float speedRate; //Speed of the bullet
    public float currentScore; //Set your scores
    [HideInInspector] public float pointsWorth = 100F; //Rewarding points that kills a tank
    public GameObject bulletPrefab; //To link to the bullet model
    //Hide tank motor and cannon from inspector
    [HideInInspector] public TankMotor motor;
    [HideInInspector] public TankCannon cannon;
    [HideInInspector] public TankHealth health;
    // Update is called once per frame
    // Update is called once per frame
    void Start()
    {
        //Update the motor and cannon.
        motor = GetComponent<TankMotor>();
        cannon = GetComponent<TankCannon>();
        health = GetComponent<TankHealth>();
    }
}
