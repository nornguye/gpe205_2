﻿//Norman Nguyen
//Tank AI: Which is the AI script for the tank which is right now just locking and shooting players.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankAI : MonoBehaviour
{
    //Target the player tank
    public Transform target;
    //Put the tank firing point (Spawn bullets) on the AI Tank
    private TankCannon cannon;
    void Start()
    {
        //Start their cannon component
        cannon = GetComponent<TankCannon>();
    }
    // Update on the tank rotate to look at the player (player tank)
    void Update()
    {
        Vector3 relativePos = target.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
        FireAtPlayer();
    }
    void FireAtPlayer()
    {
        //Fire bullets at the player as it sees
        cannon.Fire();
    }
}
