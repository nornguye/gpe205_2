﻿//Norman Nguyen
//This is the Grunt AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GruntAIController : MonoBehaviour {
    //Main AI Controller component
    public AIController controller;

    public Transform target;
    //Enum list of AI States
    public enum AIStates
    {
        Idle, Chase, Patrol
    }
    public AIStates currentState;
    //Enum List for Patrol
    public enum TankPatrol
    {
        aiStop, aiLoop, aiPingPong, aiRandom
    };
    public TankPatrol tankPatrol;
    public float timeInCurrentState;
    // Use this for initialization
    void Start ()
    {
        controller = GetComponent<AIController>();
    }
    

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case AIStates.Chase:
                Chase();
                break;
            case AIStates.Patrol:
                Patrol();
                break;
            case AIStates.Idle:
                Idle();
                break;
        }
    }
    public void ChangeState(AIStates newState)
    {
        currentState = newState;
        timeInCurrentState = 0;
    }
    //IDLE = leave it be.
    public void Idle()
    {

    }
    //Chase Method
    public void Chase()
    {

    }
    //Patrol Method
    public void Patrol()
    { 
        //Move Towards the new waypoint based from the main AI Controller
        controller.MoveTowards(controller.waypoints[controller.currentWaypoint].position);

        // If "close enough" switch to "next" waypoint
        if (Vector3.Distance(controller.data.motor.tf.position, controller.waypoints[controller.currentWaypoint].position) <= controller.waypointCutoff)
        {
            DetermineNextWayPoint();
            DetermineNextWayPointAdvanced();
        }
    }
    //Determine the next waypoint
    void DetermineNextWayPoint()
    {
        controller.currentWaypoint++;
        if (controller.currentWaypoint >= controller.waypoints.Count)
        {
             controller.currentWaypoint = 0;
        }
        // Make sure my waypoint is within range!!!
        controller.currentWaypoint = Mathf.Clamp(controller.currentWaypoint, 0, controller.waypoints.Count - 1);

    }

    void DetermineNextWayPointAdvanced()
    {
        // Advance to next waypoint
        if (tankPatrol == TankPatrol.aiPingPong)
        {
            // Ping pong is the only method that doesn't always move forward
            // Move to the "next" waypoint -- but "next" depends on what direction I'm moving.
            if (controller.isAdvancingWaypoints)
            {
                controller.currentWaypoint++;
            }
            else
            {
                controller.currentWaypoint--;
            }
        }
        else if (tankPatrol == TankPatrol.aiRandom)
        {
            int prevWaypoint = controller.currentWaypoint;
            while (prevWaypoint == controller.currentWaypoint)
            {
                controller.currentWaypoint = Random.Range(0, controller.waypoints.Count);
            }
        }
        else
        {
            controller.currentWaypoint++;
        }

        // Deal with being at the LAST waypoint
        if (controller.currentWaypoint >= controller.waypoints.Count)
        {
            if (tankPatrol == TankPatrol.aiLoop)
            {
                // Loop
                controller.currentWaypoint = 0;
            }
            else if (tankPatrol == TankPatrol.aiPingPong)
            {
                // Reverse directions
                controller.isAdvancingWaypoints = false;
                controller.currentWaypoint = controller.currentWaypoint - 2;
            }
            else if (tankPatrol == TankPatrol.aiStop)
            {
                        controller.isPatrol = false;
            }
        }
        else if (controller.currentWaypoint < 0)
        {
            // Reverse directions (to go forward again)
            controller.isAdvancingWaypoints = true;
            controller.currentWaypoint = 1;
        }
        { 
            controller.currentWaypoint++;
        }

        // Deal with being at the LAST waypoint
        if (controller.currentWaypoint >= controller.waypoints.Count)
        {
            if (tankPatrol == TankPatrol.aiLoop)
            {
                // Loop
                controller.currentWaypoint = 0;
            }
            else if (tankPatrol == TankPatrol.aiPingPong)
            {
                // Reverse directions
                controller.isAdvancingWaypoints = false;
                controller.currentWaypoint = controller.currentWaypoint - 2;
            }
            //Stop if false
            else if (tankPatrol == TankPatrol.aiStop)
            {
                controller.isPatrol = false;
            }
        }
        else if (controller.currentWaypoint < 0)
        {
            // Reverse directions (to go forward again)
            controller.isAdvancingWaypoints = true;
            controller.currentWaypoint = 1;
        }
    }
}
