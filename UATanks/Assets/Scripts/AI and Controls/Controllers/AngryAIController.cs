﻿//Norman Nguyen
//This is the Angry AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngryAIController : MonoBehaviour
{
    public TankData data;
    AIController controller;
    public enum AIStates //AI States: We need Chase and Flee for the Anger
    {
        Idle, Chase, Flee
    }
    public AIStates currentState; //varaible for AIStates
    public float timeInCurrentState; //
    public float fleeHealthPercent = .5f; //Flee Percent
    public float fleeTime = 10.0f;

    public float chaseDistance = 10.0f; //Chase Distance
    public float viewDistance = 30.0f; //View Distance
    public float hearDistance = 10.0f; //Hear Distance
    public float fieldOfView = 30.0f; //Your Field of View for the player.
    public float fleeDistance = 1.0f; //Flee Distance

    public float idleTime = 10.0f; //Idle Ti
    public Transform target;

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
    }
    // Update is called once per frame
    void Update()
    {
        //Player Transform = to the GameManager transform.
        timeInCurrentState += Time.deltaTime;
        //Switch Statement
        switch (currentState)
        {
            //Idle State
            case AIStates.Idle:
                Idle();
                break;
            //Chase State
            case AIStates.Chase:
                Chase();
                break;
        }
    }
    //Change States for the AI States enum
    void ChangeState(AIStates newState)
    {
        //Set the state
        currentState = newState;
        //Timer for the new state in an instant
        timeInCurrentState = 0;
    }
    //IDLE = leave it be.
    void Idle()
    {

    }
    //Chase Method
    void Chase()
    {

        controller.MoveTowards(GameManager.instance.player.data.motor.tf.position);
    }
    //Flee from the player tank
    public void Flee()
    {
        //Get the vector from the player
        Vector3 vectorToPlayer = GameManager.instance.player.data.motor.transform.position - data.motor.tf.position;
        vectorToPlayer = -1 * vectorToPlayer;
        //Back to Normal State
        vectorToPlayer.Normalize();
        //Move towards a new point
        controller.MoveTowards(vectorToPlayer);
    }
    //Chase Method
    //Can Hear the Player
    bool CanHear()
    {
        //float distance for the view for the tank
        float distance = Vector3.Distance(data.motor.tf.position, GameManager.instance.player.data.motor.transform.position);
        return distance < hearDistance;
    }
    //Have the AI to see.
    bool CanSee(GameObject target)
    {
        //FOV
        Vector3 vectorToTarget = GameManager.instance.player.data.motor.transform.position - data.motor.tf.position;
        float Angle = Vector3.Angle(vectorToTarget, data.motor.tf.forward);
        if (Angle > fieldOfView)
        {
            return false;
        }
        //Raycast
        RaycastHit hitInfo;
        Physics.Raycast(data.motor.tf.position, vectorToTarget, out hitInfo, viewDistance);
        if (hitInfo.collider == null)
        {
            return false;
        }

        //Collider
        Collider targetCollider = GameManager.instance.player.data.GetComponent<Collider>();
        if (targetCollider != hitInfo.collider)
        {
            return false;
        }
        return true;
    }
}

