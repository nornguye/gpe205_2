﻿//Norman Nguyen
//This is the Heavy AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyAIController : MonoBehaviour
{
    public TankData data;
    public AIController controller;
    //Target the player tank
    public Transform target;
    //Put the tank firing point (Spawn bullets) on the AI Tank
    private TankCannon cannon;
    public float viewDistance;
    public enum AIStates
    {
        Idle, Attack
    }
    public AIStates currentState;
    public float timeInCurrentState;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
        cannon = GetComponent<TankCannon>();
    }
    // Update is called once per frame
    void Update()
    {
        timeInCurrentState += Time.deltaTime;
        switch (currentState)
        {
            case AIStates.Idle:
                Idle();
                break;
            case AIStates.Attack:
                Attack();
                RaycastHit hit;
                Physics.SphereCast(data.motor.tf.position, .01f, data.motor.tf.forward, out hit, viewDistance);
                break;
        }
    }
    public void ChangeState(AIStates newState)
    {
        currentState = newState;
        timeInCurrentState = 0;
    }
    //IDLE = leave it be.
    public void Idle()
    {

    }
    
    public void Attack()
    {
        Vector3 relativePos = target.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
        FireAtPlayer();
    }
    void FireAtPlayer()
    {
        //Fire bullets at the player as it sees
        cannon.Fire();
    }
    //Flee from the player tank
    //public void Flee()
    //{
    //    Vector3 vectorToPlayer = controller.target.position + transform.position;
    //    vectorToPlayer = -1 * vectorToPlayer;
    //    //Back to Normal State
    //    vectorToPlayer.Normalize();
    //    controller.MoveTowards(vectorToPlayer);
    //}
    //public void Chase()
    //{
    //    controller.MoveTowards(controller.target.position);
    //}
}
