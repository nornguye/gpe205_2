﻿//Norman Nguyen
//This is the main AI Controller for the 4 AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    //****Tank Components****
    [HideInInspector]public TankData data;
    //Transform Array
    public List<Transform> waypoints;
    public int currentWaypoint = 0; //currentWaypoints
    public float waypointCutoff;
    public bool isPatrol = true;
    public bool isAdvancingWaypoints = true;
    //****AVOIDANCE****
    public enum AvoidState
    {
        Normal, TurnToAvoid, MoveToAvoid
    }
    public AvoidState avoidState = AvoidState.Normal;
    public float avoidDuration = 1.0f;
    private float avoidStateTime;
    public float avoidDistance = 2.0f;
    //Put the tank firing point (Spawn bullets) on the AI Tank

    void Start()
    {
        data = GetComponent<TankData>();
    }
    //Update based on
    void Update()
    {

    }
    //Can Move Forward
    bool CanMoveForward()
    {
        //Raycast is a view based on the.
        if (Physics.Raycast(data.motor.tf.position, data.motor.tf.forward, avoidDistance))
        {
            //false if 
            return false;
        }
        //true
        return true;
    }
    //Tank Move Forward
    public void MoveTowards(Vector3 target)
    {
        //Normal State
        if (avoidState == AvoidState.Normal)
        {
            if (CanMoveForward())
            {
                Vector3 targetPosition = new Vector3(target.x, data.motor.tf.position.y, target.z);
                Vector3 dirToWaypoint = targetPosition - data.motor.tf.position;
                //TurnTowards to the Way Point.
                data.motor.TurnTowards(dirToWaypoint);
                //Move Forward based from the data (speed) and motor (forward formula)
                data.motor.Move(data.motor.tf.forward);
            }
           
            else
            {
                avoidState = AvoidState.TurnToAvoid;
            }
        }
        //
        else if (avoidState == AvoidState.TurnToAvoid)
        {
            data.motor.Turn(1);
            //Move Forward
            if (CanMoveForward())
            {
                avoidState = AvoidState.MoveToAvoid;
                avoidStateTime = Time.time;
            }
        }
        //Move to Avoid meaning you're moving away from the obstacle
        else if (avoidState == AvoidState.MoveToAvoid)
        {
            //Can Move Forward
            if (CanMoveForward())
            {
                data.motor.Move(data.motor.tf.forward);
            }
            //Turn to Avoid
            else
            {
                avoidState = AvoidState.TurnToAvoid;
            }

            if (Time.time >= avoidStateTime + avoidDuration)
            {
                avoidState = AvoidState.Normal;
            }
        }
    }
}