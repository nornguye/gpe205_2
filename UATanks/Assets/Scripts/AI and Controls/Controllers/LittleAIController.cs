﻿//Norman Nguyen
//This is the Little Guy AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AIController))]
public class LittleAIController : MonoBehaviour {
    public TankData data;
    public AIController controller;
    public Transform target;
    public enum AIStates
    {
        Idle, Flee, Chase
    }
    public AIStates currentState;
    public float timeInCurrentState;
    public float fleeHealthPercent = .5f;
    public float chaseDistance = 10;
    public float viewDistance = 30.0f;
    public float hearDistance = 10.0f;
    public float fieldOfView = 30.0f;


    public float fleeTime = 10;
    Transform aiPosition;
    Transform playerPosition;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
    }
	// Update is called once per frame
	void Update ()
    {
        timeInCurrentState += Time.deltaTime;
        switch (currentState)
        {
            case AIStates.Idle:
                Idle();
                if (CanSee(GameManager.instance.player.gameObject))
                {
                    if (Vector3.Distance(data.motor.tf.position, GameManager.instance.player.data.motor.tf.position) <
                        chaseDistance)
                    {
                        ChangeState(AIStates.Chase);
                    }
                }
                break;
            case AIStates.Chase:
                Chase();
                if ((data.health.currentHealth / data.health.maxHealth) < fleeHealthPercent)
                {
                    ChangeState(AIStates.Flee);
                }
                break;
            case AIStates.Flee:
                Flee();
                if (CanSee(GameManager.instance.player.gameObject))
                { 
                    if (timeInCurrentState > fleeTime)
                    {
                        ChangeState(AIStates.Idle);
                    }
                }
                break;
        }
    }
    public void ChangeState(AIStates newState)
    {
        currentState = newState;
        timeInCurrentState = 0;
    }
    public void Idle()
    {

    }
    public void Chase()
    {
        controller.MoveTowards(GameManager.instance.player.data.motor.tf.position);
    }
    public void Flee()
    {
        Vector3 vectorToPlayer = GameManager.instance.player.data.motor.tf.position - data.motor.tf.position;
        vectorToPlayer = -1 * vectorToPlayer;
        //Back to Normal State
        vectorToPlayer.Normalize();
        controller.MoveTowards(vectorToPlayer);
    }
    bool CanHear()
    {
        //float distance for the view for the tank
        float distance = Vector3.Distance(data.motor.tf.position, GameManager.instance.player.data.motor.transform.position);
        return distance < hearDistance;
    }
    //Have the AI to see.
    bool CanSee(GameObject target)
    {
        //FOV
        Vector3 vectorToTarget = GameManager.instance.player.data.motor.transform.position - data.motor.tf.position;
        float Angle = Vector3.Angle(vectorToTarget, data.motor.tf.forward);
        if (Angle > fieldOfView)
        {
            return false;
        }
        //Raycast
        RaycastHit hitInfo;
        Physics.Raycast(data.motor.tf.position, vectorToTarget, out hitInfo, viewDistance);
        if (hitInfo.collider == null)
        {
            return false;
        }

        //Collider
        Collider targetCollider = GameManager.instance.player.data.GetComponent<Collider>();
        if (targetCollider != hitInfo.collider)
        {
            return false;
        }
        return true;
    }
}
